
ALTER TABLE orgut.isyeri DROP CONSTRAINT isyeri_isyiscsend_fkey;
ALTER TABLE orgut.hareket SET WITHOUT OIDS;
ALTER TABLE orgut.tespitcevap SET WITHOUT OIDS;
--***isyeri tablosuna
ALTER TABLE orgut.isyeri ADD email varchar(150) NULL;
ALTER TABLE orgut.isyeri ALTER COLUMN bcmdosyano TYPE character varying(150);
--***uye tablosuna
--**1.telefon, 2.email, 3.aciklama
ALTER TABLE orgut.uye ADD COLUMN telefon character varying(35);
ALTER TABLE orgut.uye ADD COLUMN email character varying(100);
ALTER TABLE orgut.uye ADD aciklama varchar(500) NULL;

---***tarih ayarını değiştir bizim veri tabanı için çalışsın
ALTER DATABASE "eSendika" Set datestyle To "ISO, MDY";

ALTER TABLE orgut.sendika
   ADD COLUMN "tblStatus" integer DEFAULT 1;

--***Some creates
create function orgut.isyerisonhareketler_new(isyeri integer, cins integer, tarih date)
  returns SETOF orgut.hareket
stable
language sql
as $$
--isyeri : hangi isyeri
    --cins : hareket cinsi
    --tarih bu tarihten onceki hareketler
with
isyerihareket as 
    (select distinct uyeid from orgut.hareket where isyeriid=$1 and cins=$2 and tarih <= $3),
uyemaxtarih as 
    (select uyeid,max(tarih) as tarih from isyerihareket join orgut.hareket using (uyeid)  where tarih <= $3 group by uyeid ),
maxcins as 
    (select uyeid, tarih, max(cins) as cins from uyemaxtarih join orgut.hareket   using (uyeid,tarih) group by uyeid,tarih),
maxhareket as 
    (select uyeid, max(hareketid) as hareketid from maxcins join orgut.hareket using(uyeid,tarih,cins) group by uyeid)

select 
    hareket.* 
from 
    maxhareket 
        join orgut.hareket using(hareketid)
 where 
    isyeriid=$1 and cins=$2

$$;

create function orgut.subesonhareketler_new(subeid integer, cins integer, tarih date)
  returns SETOF orgut.hareket
stable
language sql
as $$
--isyeri : hangi isyeri
    --cins : hareket cinsi
    --tarih bu tarihten onceki hareketler
with
isyerihareket as
    (select distinct uyeid from orgut.hareket join orgut.isyeri using (isyeriid) where subeid=$1 and cins=$2 and tarih <= $3),
uyemaxtarih as
    (select uyeid,max(tarih) as tarih from isyerihareket join orgut.hareket using (uyeid)  where tarih <= $3 group by uyeid ),
maxcins as
    (select uyeid, tarih, max(cins) as cins from uyemaxtarih join orgut.hareket   using (uyeid,tarih) group by uyeid,tarih),
maxhareket as
    (select uyeid, max(hareketid) as hareketid from maxcins join orgut.hareket using(uyeid,tarih,cins) group by uyeid)

select
    orgut.hareket.*
from
    maxhareket
        join orgut.hareket using(hareketid)
            join orgut.isyeri using(isyeriid)

 where
    subeid=$1 and cins=$2

$$;

CREATE OR REPLACE FUNCTION orgut.genelsonhareketler_new(
	cins integer,
	tarih date)
    RETURNS SETOF orgut.hareket 
    LANGUAGE 'sql'

    COST 100
    STABLE 
    ROWS 1000
AS $BODY$
    --isyeri : hangi isyeri
    --cins : hareket cinsi
    --tarih bu tarihten onceki hareketler
with
isyerihareket as 
    (select distinct uyeid from orgut.hareket where cins=$1 and tarih <= $2),
uyemaxtarih as 
    (select uyeid,max(tarih) as tarih from isyerihareket join orgut.hareket using (uyeid)  where tarih <= $2 group by uyeid ),
maxcins as 
    (select uyeid, tarih, max(cins) as cins from uyemaxtarih join orgut.hareket   using (uyeid,tarih) group by uyeid,tarih),
maxhareket as 
    (select uyeid, max(hareketid) as hareketid from maxcins join orgut.hareket using(uyeid,tarih,cins) group by uyeid)

select 
    hareket.* 
from 
    maxhareket
        join orgut.hareket using(hareketid) 
            
 where 
    cins=$1

$BODY$;

ALTER FUNCTION orgut.genelsonhareketler_new(integer, date)
    OWNER TO birmetal_user;


-- Table: orgut."TopluIsSozlesme"

-- DROP TABLE orgut."TopluIsSozlesme";

CREATE TABLE orgut."TopluIsSozlesme"
(
    id integer NOT NULL DEFAULT nextval('orgut."TopluIsSozlesme_id_seq"'::regclass),
    "Title" character varying(150) COLLATE pg_catalog."default",
    "SezonNumarasi" integer NOT NULL,
    "Document" character varying(500) COLLATE pg_catalog."default",
    "Description" character varying(500) COLLATE pg_catalog."default",
    isyeriid integer,
    "Startdate" timestamp with time zone,
    "Enddate" timestamp with time zone,
    CONSTRAINT "TopluIsSozlesme_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE orgut."TopluIsSozlesme"
    OWNER to postgres;



drop index orgut.ssk_no_idx cascade;

update orgut.isyeri set bcmdosyano = sskno